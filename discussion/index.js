console.log("Hello from JS");

// SECTION - CONTROL STRUCTURES

// 1.1 IF-ELSE STATEMENT

// let numA = -1;

// if statement (branch)
// the task of the if statement is to execute a procedure/action if the specified condition is true.
/*if (numA <= 0) {
	// truthy branch
	// this block of code will run if the condition is MET.
	console.log("The condition was met!")
};

let name = "Lorenzo";

if (name === "Lorenzo") {
	console.log("You can proceed")
}
*/
// else statement
// will be executed is all if statement/s are not met.

// lets a create control structure that will allow us to simulate a user login.

// prompt box - prompt()
// syntax: prompt(text/message[required], placeholder/default text);
// prompt("Please enter your first name","Your name");


// lets create a function that will allows us to check if the user is old enough to drink

/*
let age = 100;

if(age >= 18) {
	alert("You're old enough to drink.");
} else {
	// falsy branch
	alert("Come back another day");
};
*/

// lets create a control structure that will ask for the number of drinks that you will order
// ask the user how many drinks he wants
/*
let order = prompt("How many orders of drinks would you like?",)
// order = parseInt(order);

if (order > 0) {
	let beer = "🍻"
	alert(beer.repeat(order));
};
*/

// mini task
// vaccine checker

function vaccineChecker() {
	// ask information from the user.
	let vaxPrompt = prompt("What is your Covax?");
	// pfizer, PFIZER
	// toLowerCase() - converts a string to all lowercase characters
	vaxPrompt = vaxPrompt.toLowerCase()
	if (vaxPrompt === "pfizer" ||vaxPrompt ===  "moderna" ||vaxPrompt ===  "astrazenica" ||vaxPrompt ===  "astra zenica" ||vaxPrompt ===  "jansen") {
		alert(vaxPrompt + " vaccine is allowed to travel")
	} else {
		alert("Sorry, not permitted.")
	}
}

// we want the user to be able to invoke this function with the use of a trigger

// typhoon checker
function determineTyphoonIntensity() {
	let windspeed = parseInt(prompt("Windspeed: "))
	console.log(typeof windspeed)
	if (windspeed <= 29) {
		alert("Not a typhoon yet.")
	} else if (windspeed <= 61) {
		alert("Tropical depression detected")
	} else if (windspeed <= 88) {
		alert("Tropical storm detected")
	} else if (windspeed <= 117) {
		alert("Severe tropical storm detected")
	} else {
		alert("Typhoon Detected")
	}
};

// conditional ternary operator

// still follows the same syntax with if else statement
// ? - this would describe a condition that if resulted to "true" will run truthy
// :
function ageChecker() {
	let age = parseInt(prompt("How old are you?"))
	// ternary operator version
	return (age >= 18) ? alert("Old enough to vote."): alert("Not yet old enough.")
/*	if (age >= 18) {
		alert("Old enough to vote.")
	} else {
		alert("Not yet old enough.")
	}*/
};

// switch statements
function determineComputerOwner(){
	// unit - represents the unit number
	let unit = prompt("What is the unit no. ?")// string
	// manggagamit - represents the user who uses the unit.
	let manggagamit;
	switch (unit) {
		case "1":
			manggagamit = "John Travolta";
			break;
		case "2":
			manggagamit = "Steve Jobs"
			break;
		case "3":
			manggagamit = "Sid Meier";
			break;
		case "4":
			manggagamit = "Onel de Guzman";
			break;
		case "5":
			manggagamit = "Christian Dior";
			break;
	//if all else fails or if non of the preceeding cases above meets the expression, this statement will become the fail safe/default response.
		default:
			manggagamit = "wala yan sa options"	
	}
	console.log(manggagamit);
};

