console.log("Hello World!")

function getNumber() {
	let num1 = parseInt(prompt("What is the first number?"));
	let num2 = parseInt(prompt("What is the second number?"));
	let sum = num1 + num2;

	if (sum < 10) {
		let sum = num1 + num2;
		alert('The sum of two numbers is: ' + sum);	
	}

	else if (sum <= 20) {
		let difference = num1 - num2;
		alert('The difference of two numbers is: ' + difference);
	}

	else if (sum <= 29) {
		let product = num1 * num2;
		alert('The product of two numbers is: ' + product);
	}

	else if (sum >= 30) {
		let quotient = num1 / num2;
		alert('The quotient of two numbers is: ' + quotient);
	}

}

function userInfo() {
	let name = prompt("What is your name?");
	let age = prompt("How old are you?");

	if (name == "" || age == "") {
		alert("Are you a time traveler?");
	}
	else {
		alert(`${name} is ${age} years old`);
	}
}

function ageChecker() {
	let age = prompt("What is your age again?");
	let response;

	switch (age) {
		case "18":
		response = "You are now allowed to party.";
		break;
		case "21":
		response = "You are now part of the society.";
		break;
		case "65":
		response = "We thank you for contributing to society."
		break;
		default:
		response = "Are you sure you're not an alien?"
	}
	alert(response);


}

//create a function that will determine if the age is too old for preschool
function ageChecker(){
  //we are going to use a try-catch statement instead

  //get the input of the user.
  //the getElementById() will target a component within the document using its ID attribute
  //the "document" parameter describes the HTML document/file where the JS module is linked.
  // "value" => describes the value property of our elements
  let userInput = document.getElementById('age').value; 
  //alert(userInput); //checker
  //we will now target the element where we will display the output of this function.
  let message = document.getElementById('outputDisplay'); 
  console.log(typeof userInput); //string
  
  try {
  	//lets make sure that the input inserted by the user is NOT equals to a blank string.
  	//throw -> this statement examines the input and returns an error.
  	if (userInput === '' ) throw 'the input is empty';
  	//create a conditional statement that will check if the input in NOT a number 
  	//in order to check if the value is NOT A NUMBER, We will use a isNaN()
  	if (isNaN(userInput)) throw 'the input is Not a Number';
  	if (userInput <= 0) throw 'Not a valid Input' 
  	if (userInput <= 7) throw 'the input is good for preschool'; 
  	if (userInput > 7) throw 'too old for preschool';
  } catch(err) {
     //the "err" is to define the error that will be thrown by the try section. so "err" is caught by the catch statement and a custom error message will be displayed.
     //how are we going to inject a value inside the html container?
     //=> using innerHTML property : 
     //syntax: element.innerHTML -> This will allow us to get/set the HTML markup contained within the element
     message.innerHTML = "Age Input: " + err; 
  } finally {
  	//this statement here will be executed regardless of the result above.
  	console.log('This is from the finally section');
  	//lalabas both ung block of code indicated in the finally section including the outcome of the try statement.
  }

}


